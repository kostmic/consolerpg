using System;
using Xunit;
using ConsoleRPG;

namespace ConsoleRPGTests
{
  public class WeaponTests
  {
    [Fact]
    public void Create_WeaponHasInfo_Weapon()
    {
      //Arrange
      string expectedName = "Rusanovs Axe";
      EquipmentSlot expectedSlot = EquipmentSlot.Weapon;
      double expectedBaseDamage = 12;
      double expectedAttacksPerSecond = 1.55;
      WeaponGroup expectedWeaponGroup = WeaponGroup.Axe;
      Weapon w1 = new Weapon("Rusanovs Axe", 1, EquipmentSlot.Weapon, 12, 1.55, WeaponGroup.Axe);
      //Act
      string actualName = w1.Name;
      EquipmentSlot actualSlot = w1.EquipmentSlot;
      double actualBaseDamage = w1.Damage;
      double actualAttacksPerSecond = w1.AttacksPerSecond;
      WeaponGroup actualWeaponGroup = w1.WeaponGroup;
      //Assert
      Assert.Equal(expectedName, actualName);
      Assert.Equal(expectedSlot, actualSlot);
      Assert.Equal(expectedBaseDamage, actualBaseDamage);
      Assert.Equal(expectedAttacksPerSecond, actualAttacksPerSecond);
      Assert.Equal(expectedWeaponGroup, actualWeaponGroup);
    }
  }
}

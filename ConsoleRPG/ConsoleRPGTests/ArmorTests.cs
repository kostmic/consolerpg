using Xunit;
using ConsoleRPG;
using ConsoleRPG.EquipmentClasses;

namespace ConsoleRPGTests
{
  public class ArmorTests
  {
    [Fact]
    public void Create_ArmorHasInfo_Armor()
    {
      //Arrange
      string expectedName = "Chainmail";
      EquipmentSlot expectedSlot = EquipmentSlot.Body;
      int expectedVit = 30;
      int expectedStr = 2;
      int expectedDex = 5;
      int expectedIntel = 10;
      ArmorGroup expectedWeaponGroup = ArmorGroup.Mail;

      Armor a1 = new Armor("Chainmail", 1, EquipmentSlot.Body,30,2,5,10, ArmorGroup.Mail);
      //Act
      string actualName = a1.Name;
      EquipmentSlot actualSlot = a1.EquipmentSlot;
      int actualVit = a1.ArmorStats.vitality;
      int actualStr = a1.ArmorStats.strength;
      int actualDex = a1.ArmorStats.dexterity;
      int actualIntel = a1.ArmorStats.intelligence;
      ArmorGroup actualArmorGroup = a1.ArmorGroup;
      //Assert
      Assert.Equal(expectedName, actualName);
      Assert.Equal(expectedSlot, actualSlot);
      Assert.Equal(expectedVit, actualVit);
      Assert.Equal(expectedStr, actualStr);
      Assert.Equal(expectedDex, actualDex);
      Assert.Equal(expectedIntel, actualIntel);
      Assert.Equal(expectedWeaponGroup, actualArmorGroup);
    }
  }
}

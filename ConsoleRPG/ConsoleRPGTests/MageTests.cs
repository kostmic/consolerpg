using System;
using Xunit;
using ConsoleRPG;
using System.Collections.Generic;
using ConsoleRPG.EquipmentClasses;

namespace ConsoleRPGTests
{
  public class MageTests
  {
    [Fact]
    public void GetName_Mage()
    {
      //Arrange
      Mage w1 = new Mage("Jim");

      //Act
      var actual = w1.Name;

      //Assert
      Assert.Equal("Jim", actual);
    }


    [Fact]
    public void Create_MageHasPrimaryStats_Mage()
    {
      //Arrange
      Mage w1 = new Mage("Jim");
      int expectedVit = 5;
      int expectedStr = 1;
      int expectedDex = 1;
      int expectedIntel = 8;
      //Act
      int actualVit = w1.basePrimaryAttributes.vitality;
      int actualStr = w1.basePrimaryAttributes.strength;
      int actualDex = w1.basePrimaryAttributes.dexterity;
      int actualIntel = w1.basePrimaryAttributes.intelligence;
      //Assert
      Assert.Equal(expectedVit, actualVit);
      Assert.Equal(expectedStr, actualStr);
      Assert.Equal(expectedDex, actualDex);
      Assert.Equal(expectedIntel, actualIntel);
    }

    [Fact]
    public void LevelUp_IncreaseLevelByOne_Mage()
    {
      //Arrange
      Mage w1 = new Mage("Jim");
      //Act
      w1.LevelUp();
      //Assert
      Assert.Equal(2, w1.Level);
    }

    [Fact]
    public void LevelUp_IncreaseBaseStats_Mage()
    {
      //Arrange
      Mage m1 = new Mage("Jim");
      int expectedVit = 5 + 3;
      int expectedStr = 1 + 1;
      int expectedDex = 1 + 1;
      int expectedIntel = 8 + 5;
      //Act
      m1.LevelUp();
      int actualVit = m1.basePrimaryAttributes.vitality;
      int actualStr = m1.basePrimaryAttributes.strength;
      int actualDex = m1.basePrimaryAttributes.dexterity;
      int actualIntel = m1.basePrimaryAttributes.intelligence;
      //Assert
      Assert.Equal(expectedVit, actualVit);
      Assert.Equal(expectedStr, actualStr);
      Assert.Equal(expectedDex, actualDex);
      Assert.Equal(expectedIntel, actualIntel);
    }

    [Fact]
    public void Create_MageHasSecondaryStats_Mage()
    {
      //Arrange
      Mage m1 = new Mage("Jim");
      int expectedHealth = 5 * 10;
      int expectedAR = 1 + 1;
      int expectedER = 8;
      //Act
      int actualHealth = m1.secondaryAttributes.health;
      int actualAR = m1.secondaryAttributes.armorRating;
      int actualER = m1.secondaryAttributes.elementalResistance;
      //Assert
      Assert.Equal(expectedHealth, actualHealth);
      Assert.Equal(expectedAR, actualAR);
      Assert.Equal(expectedER, actualER);
    }

    [Fact]
    public void Equip_MageHasEquippedWeapon_Mage()
    {
      //Arrange
      Mage m1 = new Mage("Jim");
      Weapon w1 = new Weapon("Wooden Wand", 1, EquipmentSlot.Weapon, 12, 1.55, WeaponGroup.Wand);
      m1.Equip(w1);
      //Act
      Dictionary<EquipmentSlot, Equipment> actual = m1.EquippedItems;
      Dictionary<EquipmentSlot, Equipment> expected = new Dictionary<EquipmentSlot, Equipment>();
      expected.Add(EquipmentSlot.Head, null);
      expected.Add(EquipmentSlot.Body, null);
      expected.Add(EquipmentSlot.Legs, null);
      expected.Add(EquipmentSlot.Weapon, null);
      expected[EquipmentSlot.Weapon] = w1;
      //Assert
      Assert.Equal(actual, expected);
    }

    [Fact]
    public void Equip_MageSwapsWeapon_Mage()
    {
      //Arrange
      Mage m1 = new Mage("Jim");
      Weapon w1 = new Weapon("Dazzeling Wand", 1, EquipmentSlot.Weapon, 12, 1.55, WeaponGroup.Wand);
      Weapon w2 = new Weapon("Corroded War Staff", 1, EquipmentSlot.Weapon, 5, 1.2, WeaponGroup.Staff);
      Dictionary<EquipmentSlot, Equipment> expected = new Dictionary<EquipmentSlot, Equipment>();
      expected.Add(EquipmentSlot.Head, null);
      expected.Add(EquipmentSlot.Body, null);
      expected.Add(EquipmentSlot.Legs, null);
      expected.Add(EquipmentSlot.Weapon, null);
      expected[EquipmentSlot.Weapon] = w2;
      //Act
      m1.Equip(w1);
      m1.Equip(w2);
      Dictionary<EquipmentSlot, Equipment> actual = m1.EquippedItems;
      //Assert
      Assert.Equal(actual, expected);
    }

    [Fact]
    public void Equip_MageTriesToEquipWrongTypeWeapon_Mage()
    {
      //Arrange
      Mage m1 = new Mage("Jim");
      Weapon w1 = new Weapon("Rusanovs Axe", 1, EquipmentSlot.Weapon, 12, 1.55, WeaponGroup.Axe);
      //Act & Assert
      Assert.Throws<InvalidWeaponException>(() => m1.Equip(w1));
    }

    [Fact]
    public void Equip_MageTriesToEquipTooHighLevelWeapon_Mage()
    {
      //Arrange
      Mage m1 = new Mage("Jim");
      Weapon w1 = new Weapon("Rusanovs Axe", 3, EquipmentSlot.Weapon, 12, 1.55, WeaponGroup.Axe);
      //Act & Assert
      Assert.Throws<InvalidWeaponException>(() => m1.Equip(w1));
    }

    [Fact]
    public void Equip_IncreaseDPSWhenEquipWeapon_Mage()
    {
      //Arrange
      double expectedDPS = (50 * 1.55) * 1 + 8 / 100;
      Mage m1 = new Mage("Jim");
      Weapon w1 = new Weapon("Dazzeling Wand", 1, EquipmentSlot.Weapon, 50, 1.55, WeaponGroup.Wand);
      //Act
      m1.Equip(w1);
      double actualDPS = m1.DPS;

      //Assert
      Assert.Equal(expectedDPS, actualDPS);
    }

    [Fact]
    public void Equip_EquipNewWeaponChangeDPS_Mage()
    {
      //Arrange
      double expectedDPS = (70 * 1.2) * 1 + 8 / 100;
      Mage m1 = new Mage("Jim");
      Weapon w1 = new Weapon("Dazzeling Wand", 1, EquipmentSlot.Weapon, 50, 1.55, WeaponGroup.Wand);
      Weapon w2 = new Weapon("Corroded War Staff", 1, EquipmentSlot.Weapon, 70, 1.2, WeaponGroup.Staff);
      //Act
      m1.Equip(w1);
      m1.Equip(w2);
      double actualDPS = m1.DPS;

      //Assert
      Assert.Equal(expectedDPS, actualDPS);
    }

    [Fact]
    public void Equip_MageCheckEquippedArmorCheck_Mage()
    {
      //Arrange
      string expected = "New armor equipped!";
      Mage m1 = new Mage("Jim");
      Armor a1 = new Armor("Cloth vest", 1, EquipmentSlot.Body, 24, 4, 1, 10, ArmorGroup.Cloth);
      //Act
      string actual = m1.Equip(a1);
      //Assert
      Assert.Equal(expected, actual);
    }


  }
}

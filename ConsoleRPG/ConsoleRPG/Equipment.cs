using System;

namespace ConsoleRPG
{
  public enum EquipmentSlot
  {
    Head,
    Body,
    Legs,
    Weapon
  }
  public abstract class Equipment
  {
    public string Name { get; set; }
    public int RequiredLevel { get; set; }
    public EquipmentSlot EquipmentSlot;

    public Equipment(string Name, int RequiredLevel, EquipmentSlot EquipmentSlot)
    {
      this.Name = Name;
      this.RequiredLevel = RequiredLevel;
      this.EquipmentSlot = EquipmentSlot;

    }
  }
}

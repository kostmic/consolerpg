namespace ConsoleRPG
{
  public struct SecondaryAttributes
  {
    public int health { get; set; }
    public int armorRating { get; set; }
    public int elementalResistance { get; set; }
    public SecondaryAttributes(int health, int armorRating, int elementalResistance)
    {
      this.health = health;
      this.armorRating = armorRating;
      this.elementalResistance = elementalResistance;
    }
  }
}

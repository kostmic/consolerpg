using ConsoleRPG.EquipmentClasses;

namespace ConsoleRPG.CharacterClasses
{
  class Rogue : Character
  {
    public Rogue(string name) : base(name)
    {
      SetPrimaryAttributes(8, 2, 6, 1);
      CalculateSecondaryAttributes();
    }
    /// <summary>
    /// This function takes a weapon type as a parameter and equips after it has unequipped any previously equipped items.
    /// </summary>
    /// <param name="weapon"></param>
    /// <returns></returns>
    public new string Equip(Weapon weapon)
    {
      if ((weapon.WeaponGroup == WeaponGroup.Dagger || weapon.WeaponGroup == WeaponGroup.Sword) && weapon.RequiredLevel <= Level)
      {
        base.Equip(weapon);
        CalculateDPS(totalPrimaryAttributes.dexterity);
        return "New weapon equipped";
      }
      else
      {
        if (weapon.RequiredLevel > Level)
        {
          throw new InvalidWeaponException("Level is too low to equip this item. " +
            "\nCurrent level: " + Level
            + "\nLevel required to equip weapon :" + weapon.RequiredLevel);
        }
        else
        {
          throw new InvalidWeaponException("Rogue cannot equip" + weapon.WeaponGroup + ".");
        }
      }
    }
    /// <summary>
    /// This function takes an armor type as a parameter and equips it after it has unequipped any previously equipped items.
    /// </summary>
    /// <param name="armor"></param>
    /// <returns></returns>
    public new string Equip(Armor armor)
    {
      if ((armor.ArmorGroup == ArmorGroup.Leather || armor.ArmorGroup == ArmorGroup.Mail) && armor.RequiredLevel <= Level)
      {
        base.Equip(armor);
        CalculateDPS(totalPrimaryAttributes.dexterity);
        return "New armor equipped!";
      }
      else
      {
        if (armor.ArmorGroup != ArmorGroup.Leather || armor.ArmorGroup != ArmorGroup.Mail)
        {
          throw new InvalidArmorException("Rogue cannot equip " + armor.ArmorGroup + " type armor.");
        }
        else
        {
          throw new InvalidArmorException("Level is too low to equip this item. " +
            "\nCurrent level: " + Level
            + "\nLevel required to equip weapon :" + armor.RequiredLevel);
        }
      }
    }
    /// <summary>
    /// This function is used to level up the character. Within the function body the scaling attributes are passed, and the character DPS is recalculated.
    /// </summary>
    public override void LevelUp()
    {
      basePrimaryAttributes.Add(3, 1, 4, 1);
      base.LevelUp();
      CalculateDPS(totalPrimaryAttributes.dexterity);
    }
  }
}

using ConsoleRPG.EquipmentClasses;

namespace ConsoleRPG
{
  public class Mage : Character
  {
    public Mage(string name) : base(name)
    {
      SetPrimaryAttributes(5, 1, 1, 8);
      CalculateSecondaryAttributes();
    }
    /// <summary>
    /// This function takes a weapon type as a parameter and equips it after it has unequipped any previously equipped items.
    /// </summary>
    /// <param name="weapon"></param>
    /// <returns></returns>
    public new string Equip(Weapon weapon)
    {
      if ((weapon.WeaponGroup == WeaponGroup.Staff || weapon.WeaponGroup == WeaponGroup.Wand) && weapon.RequiredLevel <= Level)
      {
        base.Equip(weapon);
        CalculateDPS(totalPrimaryAttributes.intelligence);
        return "New weapon equipped";
      }
      else
      {
        if (weapon.RequiredLevel > Level)
        {
          throw new InvalidWeaponException("Level is too low to equip this item. " +
          "\nCurrent level: " + Level
          + "\nLevel required to equip weapon :" + weapon.RequiredLevel);
        }
        else
        {
          throw new InvalidWeaponException("Mage cannot equip " + weapon.WeaponGroup + ".");
        }
      }
    }
    /// <summary>
    /// This function takes an armor type as a parameter and equips it after it has unequipped any previously equipped items.
    /// </summary>
    /// <param name="armor"></param>
    /// <returns></returns>
    public new string Equip(Armor armor)
    {
      if ((armor.ArmorGroup == ArmorGroup.Cloth) && armor.RequiredLevel <= Level)
      {
        base.Equip(armor);
        CalculateDPS(totalPrimaryAttributes.intelligence);
        return "New armor equipped!";
      }
      else
      {
        if (armor.ArmorGroup != ArmorGroup.Cloth)
        {
          throw new InvalidArmorException("Mage cannot equip " + armor.ArmorGroup + " type armor.");
        }
        else
        {
          throw new InvalidArmorException("Level is too low to equip this item. " +
            "\nCurrent level: " + Level
            + "\nLevel required to equip weapon :" + armor.RequiredLevel);
        }
      }
    }
    /// <summary>
    /// This function is used to level up the character. Within the function body the scaling attributes are passed, and the character DPS is recalculated.
    /// </summary>
    public override void LevelUp()
    {
      basePrimaryAttributes.Add(3, 1, 1, 5);
      base.LevelUp();
      CalculateDPS(totalPrimaryAttributes.intelligence);
    }
  }
}

using ConsoleRPG.EquipmentClasses;

namespace ConsoleRPG.CharacterClasses
{
  class Warrior : Character
  {
    public Warrior(string name) : base(name)
    {
      SetPrimaryAttributes(10, 5, 2, 1);
      CalculateSecondaryAttributes();
    }
    /// <summary>
    /// This function takes a weapon type as a parameter and equips it after it has unequipped any previously equipped items.
    /// </summary>
    /// <param name="weapon"></param>
    /// <returns></returns>
    public new string Equip(Weapon weapon)
    {
      if ((weapon.WeaponGroup == WeaponGroup.Axe || weapon.WeaponGroup == WeaponGroup.Hammer || weapon.WeaponGroup == WeaponGroup.Sword) && weapon.RequiredLevel <= Level)
      {
        base.Equip(weapon);
        CalculateDPS(totalPrimaryAttributes.strength);
        return "New weapon equipped";
      }
      else
      {
        if (weapon.WeaponGroup != WeaponGroup.Axe || weapon.WeaponGroup != WeaponGroup.Hammer || weapon.WeaponGroup != WeaponGroup.Sword)
        {
          throw new InvalidWeaponException("Warrior cannot equip" + weapon.WeaponGroup + ".");
        }
        else
        {
          throw new InvalidWeaponException("Level is too low to equip this item. " +
            "\nCurrent level: " + Level
            + "\nLevel required to equip weapon :" + weapon.RequiredLevel);
        }
      }
    }
    /// <summary>
    /// This function takes an armor type as a parameter and equips it after it has unequipped any previously equipped items.
    /// </summary>
    /// <param name="armor"></param>
    /// <returns></returns>
    public new string Equip(Armor armor)
    {
      if ((armor.ArmorGroup == ArmorGroup.Mail || armor.ArmorGroup == ArmorGroup.Plate) && armor.RequiredLevel <= Level)
      {
        base.Equip(armor);
        CalculateDPS(totalPrimaryAttributes.strength);
        return "New armor equipped!";
      }
      else
      {
        if (armor.RequiredLevel > Level)
        {
          throw new InvalidArmorException("Level is too low to equip this item. " +
            "\nCurrent level: " + Level
            + "\nLevel required to equip weapon :" + armor.RequiredLevel);
        }
        else
        {
          throw new InvalidArmorException("Warrior cannot equip " + armor.ArmorGroup + " type armor.");
        }
      }
    }
    /// <summary>
    /// This function is used to level up the character. Within the function body the scaling attributes are passed, and the character DPS is recalculated.
    /// </summary>
    public override void LevelUp()
    {
      basePrimaryAttributes.Add(5, 3, 2, 1);
      base.LevelUp();
      CalculateDPS(totalPrimaryAttributes.strength);
    }
  }
}

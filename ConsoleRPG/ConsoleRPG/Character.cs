using ConsoleRPG.EquipmentClasses;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleRPG
{

  public abstract class Character
  {
    public string PrimaryDamageAttribute;
    public string Name;
    public int Level;
    public double DPS;

    public Dictionary<EquipmentSlot, Equipment> EquippedItems = new Dictionary<EquipmentSlot, Equipment>();

    public PrimaryAttributes basePrimaryAttributes = new PrimaryAttributes();
    public PrimaryAttributes EquipmentPrimaryAttributes = new PrimaryAttributes();
    public PrimaryAttributes totalPrimaryAttributes = new PrimaryAttributes();
    public SecondaryAttributes secondaryAttributes = new SecondaryAttributes();
    public Character(string Name)
    {
      this.Name = Name;
      Level = 1;
      DPS = 1;
      EquippedItems.Add(EquipmentSlot.Head, null);
      EquippedItems.Add(EquipmentSlot.Body, null);
      EquippedItems.Add(EquipmentSlot.Legs, null);
      EquippedItems.Add(EquipmentSlot.Weapon, null);
    }
    /// <summary>
    /// Takes a character's initial attributes as parameters, and sets the object's base primary attributes and total primary attributes struct values
    /// </summary>
    /// <param name="vitality"></param>
    /// <param name="strength"></param>
    /// <param name="dexterity"></param>
    /// <param name="intelligence"></param>
    public void SetPrimaryAttributes(int vitality, int strength, int dexterity, int intelligence)
    {
      basePrimaryAttributes.vitality = vitality;
      basePrimaryAttributes.strength = strength;
      basePrimaryAttributes.dexterity = dexterity;
      basePrimaryAttributes.intelligence = intelligence;

      totalPrimaryAttributes.vitality = vitality;
      totalPrimaryAttributes.strength = strength;
      totalPrimaryAttributes.dexterity = dexterity;
      totalPrimaryAttributes.intelligence = intelligence;
    }
    /// <summary>
    /// Used to calculate total primary attributes. This is called after changing equipment or leveling
    /// </summary>
    public void CalculateTotalAttributes()
    {
      totalPrimaryAttributes.vitality = basePrimaryAttributes.vitality + EquipmentPrimaryAttributes.vitality;
      totalPrimaryAttributes.strength = basePrimaryAttributes.strength + EquipmentPrimaryAttributes.strength;
      totalPrimaryAttributes.dexterity = basePrimaryAttributes.dexterity + EquipmentPrimaryAttributes.dexterity;
      totalPrimaryAttributes.intelligence = basePrimaryAttributes.intelligence + EquipmentPrimaryAttributes.intelligence;
    }
    /// <summary>
    /// Used to calculate secondary attributes. This is to be called whenever the primary attributes are changed
    /// </summary>
    public void CalculateSecondaryAttributes()
    {
      secondaryAttributes.health = totalPrimaryAttributes.vitality * 10;
      secondaryAttributes.armorRating = totalPrimaryAttributes.strength + totalPrimaryAttributes.dexterity;
      secondaryAttributes.elementalResistance = totalPrimaryAttributes.intelligence;
    }
    /// <summary>
    /// Checks if weapon is equipped and modifies player DPS. Is called on any attribute change
    /// </summary>
    /// <param name="primaryStat"></param>
    public void CalculateDPS(int primaryStat)
    {
      if (EquippedItems[EquipmentSlot.Weapon] != null)
      {
        Weapon tmpwep = (Weapon)EquippedItems[EquipmentSlot.Weapon];
        DPS = tmpwep.DPS * (1 + primaryStat / 100);
      }
      else
      {
        DPS = (1 + totalPrimaryAttributes.intelligence / 100);
      }
    }

    public virtual void Equip(Weapon weapon)
    {
      //Unequip old Weapon before equipping new one
      if (EquippedItems[EquipmentSlot.Weapon] != null)
      {
        EquippedItems[EquipmentSlot.Weapon] = null;
        DPS = 1;
      }
      //Equip new weapon and modify stats
      EquippedItems[EquipmentSlot.Weapon] = weapon;
    }
    /// <summary>
    /// This function takes a weapon type as a parameter and equips after it has unequipped any previously equipped items.
    /// </summary>
    /// <param name="armor"></param>
    public virtual void Equip(Armor armor)
    {
      //Unequip old Armor
      if (EquippedItems[armor.EquipmentSlot] != null)
      {
        Armor EquippedArmor = (Armor)EquippedItems[armor.EquipmentSlot];

        EquipmentPrimaryAttributes.vitality -= EquippedArmor.ArmorStats.vitality;
        EquipmentPrimaryAttributes.strength -= EquippedArmor.ArmorStats.strength;
        EquipmentPrimaryAttributes.dexterity -= EquippedArmor.ArmorStats.dexterity;
        EquipmentPrimaryAttributes.intelligence -= EquippedArmor.ArmorStats.intelligence;

        EquippedItems[armor.EquipmentSlot] = null;
      }
      //Equip new Armor and modify stats
      EquipmentPrimaryAttributes.vitality += armor.ArmorStats.vitality;
      EquipmentPrimaryAttributes.strength += armor.ArmorStats.strength;
      EquipmentPrimaryAttributes.dexterity += armor.ArmorStats.dexterity;
      EquipmentPrimaryAttributes.intelligence += armor.ArmorStats.intelligence;
      EquippedItems[armor.EquipmentSlot] = armor;
    }

    public virtual void LevelUp()
    {
      Level++;
      CalculateTotalAttributes();
      CalculateSecondaryAttributes();
    }

    public virtual string StatsToString()
    {
      string Stats = "";

      Stats += "Name: ";
      Stats += Name;
      Stats += "\nLevel: ";
      Stats += Level;
      Stats += "\nTotal Strength: ";
      Stats += totalPrimaryAttributes.strength;
      Stats += "\nTotal Dexterity: ";
      Stats += totalPrimaryAttributes.dexterity;
      Stats += "\nTotal Intelligence: ";
      Stats += totalPrimaryAttributes.intelligence;
      Stats += "\nTotal Health: ";
      Stats += secondaryAttributes.health;
      Stats += "\nTotal Armor Rating: ";
      Stats += secondaryAttributes.armorRating;
      Stats += "\nTotal Elemental Resistance: ";
      Stats += secondaryAttributes.elementalResistance;
      Stats += "\nCharacter DPS: ";
      Stats += DPS;
      return Stats;
    }
  }
}

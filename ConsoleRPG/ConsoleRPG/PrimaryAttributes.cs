namespace ConsoleRPG
{
  public struct PrimaryAttributes
  {
    public int vitality { get; set; }
    public int strength { get; set; }
    public int dexterity { get; set; }
    public int intelligence { get; set; }
    public PrimaryAttributes(int vitality, int strength, int dexterity, int intelligence)
    {
      this.vitality = vitality;
      this.strength = strength;
      this.dexterity = dexterity;
      this.intelligence = intelligence;
    }

    public void Add(int vitality, int strength, int dexterity, int intelligence)
    {
      this.vitality += vitality;
      this.strength += strength;
      this.dexterity += dexterity;
      this.intelligence += intelligence;
    }
  }
}

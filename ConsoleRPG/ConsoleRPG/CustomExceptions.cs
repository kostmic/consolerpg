using System;

namespace ConsoleRPG
{
  [Serializable]
  public class InvalidWeaponException : Exception
  {
    public InvalidWeaponException()
    {
    }
    public InvalidWeaponException(string message)
      : base(message)
    {
    }
  }

  [Serializable]
  public class InvalidArmorException : Exception
  {
    public InvalidArmorException()
    {
    }
    public InvalidArmorException(string message)
      : base(message)
    {
    }
  }
}

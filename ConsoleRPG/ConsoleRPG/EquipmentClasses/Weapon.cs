namespace ConsoleRPG
{

  public enum WeaponGroup
  {
    Axe,
    Bow,
    Dagger,
    Hammer,
    Staff,
    Sword,
    Wand
  }
  public class Weapon : Equipment
  {
    public int Damage { get; }
    public double DPS { get; }
    public double AttacksPerSecond { get; }
    public WeaponGroup WeaponGroup { get; }
    public Weapon(string Name, int RequiredLevel, EquipmentSlot EquipmentSlot, int Damage, double AttacksPerSecond, WeaponGroup WeaponGroup) : base(Name, RequiredLevel, EquipmentSlot)
    {
      this.Damage = Damage;
      this.AttacksPerSecond = AttacksPerSecond;
      this.WeaponGroup = WeaponGroup;
      DPS = Damage * AttacksPerSecond;
    }


  }


}

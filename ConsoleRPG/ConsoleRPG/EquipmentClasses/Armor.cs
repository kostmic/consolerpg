using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.EquipmentClasses
{
  public enum ArmorGroup
  {
    Cloth,
    Leather,
    Mail,
    Plate
  }

  public class Armor : Equipment
  {
    public PrimaryAttributes ArmorStats;
    public ArmorGroup ArmorGroup { get; }

    public Armor(string Name, int RequiredLevel, EquipmentSlot EquipmentSlot,int vit, int str, int dex ,int intel, ArmorGroup ArmorGroup) : base(Name, RequiredLevel, EquipmentSlot)
    {
      this.Name = Name;
      this.RequiredLevel = RequiredLevel;
      this.EquipmentSlot = EquipmentSlot;
      ArmorStats.vitality = vit;
      ArmorStats.strength = str;
      ArmorStats.dexterity = dex;
      ArmorStats.intelligence = intel;
      this.ArmorGroup = ArmorGroup;
    }
  }
}
